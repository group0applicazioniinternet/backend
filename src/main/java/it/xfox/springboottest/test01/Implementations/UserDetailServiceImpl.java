package it.xfox.springboottest.test01.Implementations;

import it.xfox.springboottest.test01.Entities.User;
import it.xfox.springboottest.test01.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


//oAuth2 needs a class implementing ClientDetailsService and UserDetailService.
//Basically this class provide a method to loadUser (with credential) from DB or wherever
//information are stored.
//In our case those information are store into the DB.

@Service
public class UserDetailServiceImpl implements UserDetailsService{



    @Autowired
    private UserRepository userRepository;


    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    public User loadByUserId(String userid) throws UsernameNotFoundException{
        return userRepository.findById(userid);
    }
}
