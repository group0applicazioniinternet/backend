package it.xfox.springboottest.test01.Implementations;

import it.xfox.springboottest.test01.Entities.Client;
import it.xfox.springboottest.test01.Repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//oAuth2 needs a class implementing ClientDetailsService and UserDetailService.
//Basically this class provide a method to loadClient (with credential) from DB or wherever
//information are stored.
//In our case those information are store into the DB.

@Service
@Primary
public class ClientDetailServiceImpl implements ClientDetailsService{

    @Autowired
    ClientRepository clientRepository;

    //clients are in clientRepository and they are obtained in this way:
    @Override
    public ClientDetails loadClientByClientId(String id) throws ClientRegistrationException {
        //Method requested by oAuth2
        return clientRepository.findById(id);
    }
}
