package it.xfox.springboottest.test01.Entities;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.*;

public class Client implements ClientDetails {

    @Id
    public String id;

    String secret;
    Set<String> scopes;
    Set<String> authsGrantType;
    Collection<GrantedAuthority> grantedAuthorities;

    Set<String> resources;
    Set<String> registeredRedirectUri;

    Integer accessTokenValiditySeconds;
    Integer refreshTokenValiditySeconds;

    Map<String, Object> additionalInformation;


    public Client(){}

    public Client(String id, String secret, String firstScope) {
        this.id = id;
        this.secret = secret;

        this.scopes = new HashSet<String>();
        this.authsGrantType = new HashSet<String>();

        this.scopes.add(firstScope);
        this.authsGrantType.add("password");
        this.authsGrantType.add("refresh_token");

        this.grantedAuthorities = new ArrayList<GrantedAuthority>();

        //this.grantedAuthorities.add(new myGrantedAuth("auth"));
    }

    @Override
    public String getClientId() {
        return id;
    }

    @Override
    public Set<String> getResourceIds() {
        return resources;
    }

    @Override
    public boolean isSecretRequired() {
        return false;
    }

    @Override
    public String getClientSecret() {
        return secret;
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public Set<String> getScope() {
        return scopes;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return authsGrantType;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return registeredRedirectUri;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return refreshTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String s) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return additionalInformation;
    }


    public void setClientId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Set<String> getScopes() {
        return scopes;
    }

    public void setScopes(Set<String> scopes) {
        this.scopes = scopes;
    }

    public void addScope(String scope){ this.scopes.add(scope); }

    public Set<String> getAuthsGrantType() {
        return authsGrantType;
    }

    public void setAuthsGrantType(Set<String> authsGrantType) {
        this.authsGrantType = authsGrantType;
    }

    public Collection<GrantedAuthority> getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(Collection<GrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id='" + id + '\'' +
                ", secret='" + secret + '\'' +
                ", scopes=" + scopes +
                ", authsGrantType=" + authsGrantType +
                ", grantedAuthorities=" + grantedAuthorities +
                ", resources=" + resources +
                ", registeredRedirectUri=" + registeredRedirectUri +
                ", accessTokenValiditySeconds=" + accessTokenValiditySeconds +
                ", refreshTokenValiditySeconds=" + refreshTokenValiditySeconds +
                ", additionalInformation=" + additionalInformation +
                '}';
    }
}
