package it.xfox.springboottest.test01.Entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.geo.Point;

import java.util.*;

//java class for Archive information
public class Archive {
    @Id
    private String id;

    //id of the user that owns the archive
    private String userid;

    //elements in the archive (not approximate)
    private List<ArchiveElement> elements;

    //approximate info
    private List<Date> appTimestamps;
    private List<Point> appPoints;

    //counter of the times the archive has been bought
    private Integer boughtCounter;

    //if the archive has been deleted by its owner, deleted is set to true
    private boolean deleted;


    //empty constructor
    public Archive() {
        this.elements = new ArrayList<>();
        this.appTimestamps = new ArrayList<>();
        this.appPoints = new ArrayList<>();

        this.boughtCounter = 0;
        this.deleted = false;
    }


    //constructor
    public Archive(String userid) {
        this.userid = userid;

        this.elements = new ArrayList<>();
        this.appTimestamps = new ArrayList<>();
        this.appPoints = new ArrayList<>();

        this.boughtCounter = 0;
        this.deleted = false;
    }


    //add an ArchiveElement to the elements list
    //and update approximate points and timestamps
    public void addElement(ArchiveElement element) {
        this.elements.add(element);

        Date FT = element.getTimestamp(); //Full Timestamp
        Date AT = new Date(FT.getTime()); //Approx Timestamp
        AT.setSeconds(0);
        appTimestamps.add(AT);


        Double X = Math.round(element.getLocation().getX() * 100.0) / 100.0;
        Double Y = Math.round(element.getLocation().getY() * 100.0) / 100.0;
        Point AP = new Point(X, Y); //Approx Point
        appPoints.add(AP);
    }


    //sort the approximate timestamps and positions
    public void finalizeArchive(){
        this.appTimestamps.sort(Date::compareTo);

        this.appPoints.sort((p1, p2) -> {
            if(p1.getX() != p2.getX()) {
                return (int) (p1.getX() - p2.getX());
            }else if(p1.getY() != p2.getY()){
                return (int) (p1.getY() - p2.getY());
            }else{
                return 0;
            }
        });
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public List<ArchiveElement> getElements() {
        return elements;
    }

    public void setElements(List<ArchiveElement> elements) {
        this.elements = elements;
    }

    public List<Date> getAppTimestamps() {
        return appTimestamps;
    }

    public void setAppTimestamps(List<Date> appTimestamps) {
        this.appTimestamps = appTimestamps;
    }

    public List<Point> getAppPoints() {
        return appPoints;
    }

    public void setAppPoints(List<Point> appPoints) {
        this.appPoints = appPoints;
    }

    public Integer getBoughtCounter() {
        return boughtCounter;
    }

    public void setBoughtCounter(Integer boughtCounter) {
        this.boughtCounter = boughtCounter;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}