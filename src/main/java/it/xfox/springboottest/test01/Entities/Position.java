package it.xfox.springboottest.test01.Entities;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.data.geo.Point;

import java.util.Date;


//position class
public class Position {

    @Id
    private String id;

    //id of the user that uploaded this position
    private String userid;

    //coordinates of the position
    private Point location;

    //timestamp of the position
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date timestamp;

    //archiveid of the archive from which this position belongs
    private String archiveId;


    public Position() {}


    public Position(String userid, Point location, Date timestamp) {
        this.userid = userid;
        this.location = location;
        this.timestamp = timestamp;
    }


    public Position(String userid, Point location, Date timestamp, String archiveId) {

        this.userid = userid;
        this.location = location;
        this.timestamp = timestamp;
        this.archiveId = archiveId;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getUserid() {
        return userid;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }


    public Point getLocation() {
        return location;
    }


    public void setLocation(Point location) {
        this.location = location;
    }


    public Date getTimestamp() {
        return timestamp;
    }


    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }


    public String getArchiveId() { return archiveId; }


    public void setArchiveId(String archiveId) { this.archiveId = archiveId; }


    //from lab 3
    public boolean isValid(){
        if((this.location.getX() > 90) || (this.location.getX() < -90)){
            return false;
        }

        if((this.location.getY() > 180) || (this.location.getY() < -180)){
            return false;
        }

        return true;
    }


    public Double evaluateVelocityOver(Position p){
        Double distance;
        Long time;
        Double vel;

        double a = (this.location.getX() - p.location.getX())*distPerLat(this.location.getX());
        double b = (this.location.getY() - p.location.getY())*distPerLng(this.location.getY());

        distance =  Math.sqrt(a*a+b*b);
        time = this.timestamp.getTime() - p.timestamp.getTime();

        vel = (distance/time)*1000;

        return vel;
    }


    private static double distPerLng(double lat){
        return 0.0003121092*Math.pow(lat, 4)
                +0.0101182384*Math.pow(lat, 3)
                -17.2385140059*lat*lat
                +5.5485277537*lat+111301.967182595;
    }

    private static double distPerLat(double lat){
        return -0.000000487305676*Math.pow(lat, 4)
                -0.0033668574*Math.pow(lat, 3)
                +0.4601181791*lat*lat
                -1.4558127346*lat+110579.25662316;
    }



    @Override
    public String toString() {
        return "Position{" +
                "id='" + id + '\'' +
                ", userid='" + userid + '\'' +
                ", location=" + location +
                ", timestamp=" + timestamp +
                '}';
    }
}
