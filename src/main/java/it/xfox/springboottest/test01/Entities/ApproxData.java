package it.xfox.springboottest.test01.Entities;

import org.springframework.data.geo.Point;

import java.util.*;


/*
java class for the json structure returned by the viewApproxPositions function:
 "approxData": {
     "archiveIds": [
         "<archiveId>",
         ...
     ],
     "userId_appData": {
         "<userId>": {
             "appTimestamps": [
                 <appTimestamp>,
                 ...
             ],
             "appPoints": [
                 {
                     "x": <x>,
                     "y": <y>
                 },
                 ...
             ]
         }
     }
 }
*/
public class ApproxData{

    List<String> archiveIds;

    //Key = <userId> (String)
    //Value = <List of appTimestamps, List of appPoints> (SubClass)
    Map<String, SubClass> userId_appData;

    public ApproxData(){
        this.archiveIds = new ArrayList<>();
        this.userId_appData = new HashMap<>();
    }

    public void addArchiveId(String id){
        archiveIds.add(id);
    }

    public void addUserId_appData(String userid, List<Date> LD, List<Point> LP){
        SubClass SC = userId_appData.get(userid);

        if(SC == null){
            SubClass NSC = new SubClass();
            NSC.addData(LD, LP);

            userId_appData.put(userid, NSC);
        }else{
            SC.addData(LD, LP);
        }
    }

    public List<String> getArchiveIds() {
        return archiveIds;
    }

    public void setArchiveIds(List<String> archiveIds) {
        this.archiveIds = archiveIds;
    }

    public Map<String, SubClass> getUsers() {
        return userId_appData;
    }

    public void setUsers(Map<String, SubClass> users) {
        this.userId_appData = users;
    }
}


class SubClass{
    private List<Date> appTimestamps;
    private List<Point> appPoints;

    public SubClass(){
        this.appPoints = new ArrayList<>();
        this.appTimestamps = new ArrayList<>();
    };

    public void addData(List<Date> LD, List<Point> LP){
        this.appTimestamps.addAll(LD);
        this.appPoints.addAll(LP);
    }

    public List<Date> getAppTimestamps() {
        return appTimestamps;
    }

    public void setAppTimestamps(List<Date> appTimestamps) {
        this.appTimestamps = appTimestamps;
    }

    public List<Point> getAppPoints() {
        return appPoints;
    }

    public void setAppPoints(List<Point> appPoints) {
        this.appPoints = appPoints;
    }
}
