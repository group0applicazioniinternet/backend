package it.xfox.springboottest.test01.Entities;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


//user class
public class User implements UserDetails{

    //MongoDB id for the user
    @Id
    public String id;

    //credentials
    String username;
    String password;

    //from lab 3
    List<GrantedAuthority> authorityList;

    //list of archives bought
    List<String> archiveBought;


    public User(){}


    public User(String username, String password, String baseAuthority) {
        this.username = username;
        this.password = password;

        this.authorityList = new ArrayList<GrantedAuthority>();
        this.authorityList.add(new SimpleGrantedAuthority(baseAuthority));

        this.archiveBought = new ArrayList<>();
    }


    @Override
    public List<GrantedAuthority> getAuthorities() {
        return authorityList;
    }


    public void addAuthority(String authority){ this.authorityList.add(new SimpleGrantedAuthority(authority));}


    @Override
    public String getPassword() {
        return password;
    }


    @Override
    public String getUsername() {
        return username;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }


    @Override
    public boolean isAccountNonLocked() {
        return true;
    }


    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }


    @Override
    public boolean isEnabled() { return true; }


    public String getId() {
        return id;
    }



    public void setId(String id) {
        this.id = id;
    }


    public void setUsername(String username) {
        this.username = username;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public List<String> getArchiveBought() {
        return archiveBought;
    }


    public void setArchiveBought(List<String> archiveBought) {
        this.archiveBought = archiveBought;
    }


    public void addBoughtArchive(String id){
        this.archiveBought.add(id);
    }


    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
