package it.xfox.springboottest.test01.Entities;

import org.springframework.data.geo.Point;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

//element contained in the Archive object (in the elements list)
//not approximate Point and Date
public class ArchiveElement {

    private Point location;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date timestamp;

    public void setLocation(Point location) {
        this.location = location;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public ArchiveElement(Point location, Date timestamp){
        this.location = location;
        this.timestamp = timestamp;
    }


    public Point getLocation() {
        return this.location;
    }


    public Date getTimestamp() {
        return this.timestamp;
    }


    //valid coordinates, from lab 3 specifications
    public boolean isValid(){
        if((this.location.getX() > 90) || (this.location.getX() < -90)){
            return false;
        }

        if((this.location.getY() > 180) || (this.location.getY() < -180)){
            return false;
        }

        return true;
    }


    //for checks from lab 3
    public Double evaluateVelocityOver(ArchiveElement e){
        Double distance;
        Long time;
        Double vel;

        double a = (this.location.getX() - e.getLocation().getX())*distPerLat(this.location.getX());
        double b = (this.location.getY() - e.getLocation().getY())*distPerLng(this.location.getY());

        distance =  Math.sqrt(a*a+b*b);
        time = this.timestamp.getTime() - e.getTimestamp().getTime();

        vel = (distance/time)*1000;

        return vel;
    }


    private static double distPerLng(double lat){
        return 0.0003121092*Math.pow(lat, 4)
                +0.0101182384*Math.pow(lat, 3)
                -17.2385140059*lat*lat
                +5.5485277537*lat+111301.967182595;
    }


    private static double distPerLat(double lat){
        return -0.000000487305676*Math.pow(lat, 4)
                -0.0033668574*Math.pow(lat, 3)
                +0.4601181791*lat*lat
                -1.4558127346*lat+110579.25662316;
    }



    @Override
    public String toString() {
        return "ArchiveElement{" +
                "location=" + location +
                ", timestamp=" + timestamp +
                '}';
    }

}
