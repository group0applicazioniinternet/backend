package it.xfox.springboottest.test01.Utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.xfox.springboottest.test01.Entities.Archive;
import it.xfox.springboottest.test01.Entities.Position;
import it.xfox.springboottest.test01.Repositories.ArchiveRepository;
import it.xfox.springboottest.test01.Repositories.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.geo.Polygon;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class UtilFunctions {

    //find positions with coordinates in the polygon and timestamp in the time interval
    //the polygon vertices and the time interval start and end are in the myData string (json format)
    public static List<Position> retrievePossFromPolygonAndTimestamps(PositionRepository positionRepository, String myData) throws IOException, ParseException {

        //retrieve information from the json string
        ObjectMapper objectMapper = new ObjectMapper(); //create ObjectMapper instance
        JsonNode timestampRootNode = objectMapper.readTree(myData).path("timestamp"); //retrieve root node and then retrieve the timestamp node
        JsonNode polygonRootNode = objectMapper.readTree(myData).path("polygon");

        //create Date objects for start and end of the time interval
        Date timeStart = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(timestampRootNode.get("start").asText());
        Date timeEnd = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(timestampRootNode.get("end").asText());

        //create list of Point objects for the vertices of the polygon
        List<Point> points = new ArrayList<>();

        //add vertices to the list
        Iterator<JsonNode> elements = polygonRootNode.elements(); //Iterator of the elements
        while(elements.hasNext()) {
            JsonNode jn = elements.next();
            points.add(new Point(jn.get("lat").asDouble(), jn.get("long").asDouble()));
        }

        //create the Polygon object with the given vertices
        Polygon polygon = new Polygon(points);

        //retrieve the positions with coordinates in the polygon and timestamp in the time interval
        //PositionRepository function
        List<Position> posList = positionRepository.findByLocationWithinAndTimestampBetween(polygon, timeStart, timeEnd);

        return posList;
    }


    //find archives containing positions with coordinates in the polygon and timestamp in the time interval
    //the polygon vertices and the time interval start and end are in the myData string (json format)
    public static List<Archive> retrieveArchivesFromPolygonAndTimestamps(PositionRepository positionRepository, ArchiveRepository archiveRepository, String myData) throws IOException, ParseException {

        //retrieve data from the json string, as in the previous function
        ObjectMapper objectMapper = new ObjectMapper(); //create ObjectMapper instance
        JsonNode timestampRootNode = objectMapper.readTree(myData).path("timestamp"); //retrieve root node and then retrieve the timestamp node
        JsonNode polygonRootNode = objectMapper.readTree(myData).path("polygon");

        //create Date objects for start and end of the time interval
        Date timeStart = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(timestampRootNode.get("start").asText());
        Date timeEnd = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(timestampRootNode.get("end").asText());

        //create list of Point objects for the vertices of the polygon
        List<Point> points = new ArrayList<>();

        //add vertices to the list
        Iterator<JsonNode> elements = polygonRootNode.elements(); //Iterator of the elements
        while(elements.hasNext()) {
            JsonNode jn = elements.next();
            points.add(new Point(jn.get("lat").asDouble(), jn.get("long").asDouble()));
        }

        //create the Polygon object with the given vertices
        Polygon polygon = new Polygon(points);

        //retrieve the positions with coordinates in the polygon and timestamp in the time interval
        //PositionRepository function
        List<Position> positions = positionRepository.findByLocationWithinAndTimestampBetween(polygon, timeStart, timeEnd);

        //set (no duplicates) of archiveids of the archives containing the positions in the list
        Set<String> archiveSet = new HashSet<>();
        for(Position p : positions){
            archiveSet.add(p.getArchiveId());
        }

        //list of archives corresponding the the archiveids
        //no duplicates because it is derived from the set
        List<Archive> archiveList = new ArrayList<>();
        for(String archiveId : archiveSet){
            archiveList.add(archiveRepository.findById(archiveId));
        }

        return archiveList;
    }


    //This function will return a basic hashed (sha256) string.
    //It would be nice to implement other type of securities like adding salt (and pepper :D)
    //Of course this is not subject of the course soooooooooo....

    //The string will be codified in UTF_8 and for that it will be printed in an awkward way on the stdOut
    //but don't worry. It is stored correctly into the DB and parsed correctly.
    public static String hashPassword(String password) {
        String passwordHash = "";
        try {

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            passwordHash = new String(hash, StandardCharsets.UTF_8);
            return passwordHash;

        } catch (Exception e) {
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return passwordHash;
        }

    }

}