package it.xfox.springboottest.test01.Utils;

import java.util.HashMap;
import java.util.Map;

//This object will be the one and only one sent as response to any request.
//It has three default fields that represent the status of the execution of the API call.
//Success (true/false), errorcode (int), and a message string that will report the error message if any.

//Also there is a MAP where it is possible to nest elements to provide a more "resourceful" response.
//Like attaching data to the response.

//This class will always be parsed in JSON before being sent (of course)

public class Response {

    //default fields:
    public boolean success;
    public int errorcode;
    public String message;

    //additional fields:
    Map<String, Object> fields;


    public Response(){
        this.fields = new HashMap<>();
    };


    public Response(boolean success, int errorcode, String message) {
        this.success = success;
        this.errorcode = errorcode;
        this.message = message;

        //each time we want to add a field to the Response object
        //we just add an entry in this map
        this.fields = new HashMap<>();
    }


    public boolean isSuccess() {
        return success;
    }


    public void setSuccess(boolean success) {
        this.success = success;
    }


    public int getErrorcode() {
        return errorcode;
    }


    public void setErrorcode(int errorcode) {
        this.errorcode = errorcode;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public Map<String, Object> getFields() {
        return fields;
    }


    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }


    //add a new field in the Response object
    public void addField(String key, Object value){
        this.fields.put(key, value);
    }


    //set values of the default fields (success, errorcode, message)
    public void setDefaultFields(boolean success, int errorcode, String message) {
        this.success = success;
        this.errorcode = errorcode;
        this.message = message;
    }
}
