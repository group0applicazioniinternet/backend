package it.xfox.springboottest.test01;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static it.xfox.springboottest.test01.Utils.UtilFunctions.hashPassword;


//Filter all requests and change the password field (if present)
//substitute the password with the hashed password
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class PreSecurityFilter implements Filter {

    private FilterConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        config = filterConfig;
    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;

        Map<String, String[]> parameters = request.getParameterMap();
        String[] password = parameters.get("password");

        if (password != null) {
            //password[0] = password without hash
            //add the hash
            password[0] = hashPassword(password[0]);

            //set the attribute password to the new password (hash)
            request.setAttribute("password", password[0]);

        }

        //go on with the filter chain
        filterChain.doFilter(req, res);

    }
}