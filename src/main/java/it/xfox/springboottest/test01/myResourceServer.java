package it.xfox.springboottest.test01;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;


//this class is requested by Spring Security
@Configuration
@EnableResourceServer
public class myResourceServer extends ResourceServerConfigurerAdapter {

    //Just to provide a view of what it is possible to do with Spring Security
    // we leaved some commented lines here.

    //client scope (oauth)
    //private static final String WRITE_SCOPE = "#oauth2.hasScope('write')";
    private static final String READ_SCOPE = "#oauth2.hasScope('read')";

    //user authority
    private static final String USER_AUTH = "hasAuthority('USER')";


    //private static final String ADMIN_AUTH = "hasAuthority('ADMIN')";
    //private static final String CUSTOMER_AUTH = "hasAuthority('CUSTOMER')";

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/registration").permitAll()
                .antMatchers("/user/**").access(READ_SCOPE + " and " + USER_AUTH)
                //.antMatchers("/admin/**").access(READ_SCOPE + " and " + ADMIN_AUTH)
                //.antMatchers("/customer/**").access(READ_SCOPE + " and " + CUSTOMER_AUTH)

                .anyRequest().authenticated();

    }

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenServices());
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey("SecretKeyJWT");
        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }

}