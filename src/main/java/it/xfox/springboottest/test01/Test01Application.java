package it.xfox.springboottest.test01;


import it.xfox.springboottest.test01.Entities.*;
import it.xfox.springboottest.test01.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.GeospatialIndex;
import org.springframework.data.geo.Point;

import java.text.SimpleDateFormat;

import org.springframework.data.geo.Point;

import static it.xfox.springboottest.test01.Utils.UtilFunctions.hashPassword;


@SpringBootApplication
public class Test01Application implements CommandLineRunner{

    //To avoid creating default users (and few other things) every time the project is launched
    //we are creating some objects and saving them to the database after erasing everything.
    //And for that we need repositories.

	@Autowired UserRepository userRepository;
	@Autowired ClientRepository clientRepository;
	@Autowired PositionRepository positionRepository;
	@Autowired ArchiveRepository archiveRepository;

    @Autowired MongoTemplate mongoTemplate;


	public static void main(String[] args) {
	    System.out.println("Application started!");
	    SpringApplication.run(Test01Application.class, args);
	}


	@Override
    public void run(String... args) throws Exception{

        //---------- Clients ----------
        //delete all from the client repositiory
        clientRepository.deleteAll();

        //create a client and save it in the repository
        Client c1 = new Client("Client1", "Secret1", "read");
        c1.addScope("write");
        clientRepository.save(c1);

        //More clients could be created to distinguish different applications.
        //Client c2 = clientRepository.save(new Client("Client2", "Secret2", "read"));



	    //---------- Users ----------
        //delete all from the user repository
        userRepository.deleteAll();

        //create some new users and save them in the repository
        User u1 = new User("Antonio", hashPassword("pass"), "USER");
        userRepository.save(u1);

        User u2 = new User("Fausto", hashPassword("pass"), "USER");
        userRepository.save(u2);

        User u3 = new User("Sara", hashPassword("pass2"), "USER");
        userRepository.save(u3);

        User u4 = new User("Monica", hashPassword("pass2"), "USER");
        userRepository.save(u4);



        //---------- Positions ----------
        //delete all from the position repository
        positionRepository.deleteAll();

        //These lines were used for tests.
        //We thought it may be interesting

        //example of positions
        //Point corsoDuca38 = new Point(7.651939, 45.063428);
        //Point viaOsasco2 = new Point(7.660538, 45.0599);
        //(distance = 782 meters || 0.782 km || 2565 feet || 855 yards || 0.486 miles)

        //create some positions and save them in the repository
        //Corso Duca 38 @ 04/27/2018 - 9:20am (UTC)
        //positionRepository.save(new Position(u1.getId(), corsoDuca38, new java.util.Date((long)1524820800*1000)));

        //Via Osasco 2 @ 04/27/2018 - 9:26am (UTC)
        //positionRepository.save(new Position(u1.getId(), viaOsasco2, new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse("2018.04.27.9.26.00")));
        //positionRepository.save(new Position(u2.getId(), viaOsasco2, new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse("2017.04.27.9.26.00")));


        //Enabling 2d sphere index on field "location"
        //You won't be able to do geographical queries if you don't enable this index.
        mongoTemplate.indexOps(Position.class).ensureIndex( new GeospatialIndex("location") );


        //---------- Archives ----------
        //delete all from the archive repository
        archiveRepository.deleteAll();


        //debug
        System.out.println("[myOutput] DATABASE INITIALIZATION DONE ----------");
    }
}
