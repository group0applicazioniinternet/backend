package it.xfox.springboottest.test01.Repositories;

import it.xfox.springboottest.test01.Entities.Client;
import org.springframework.data.mongodb.repository.MongoRepository;


//repository with Client objects
public interface ClientRepository extends MongoRepository<Client, String> {

    //find the client with clientid = id
    public Client findById(String id);

}
