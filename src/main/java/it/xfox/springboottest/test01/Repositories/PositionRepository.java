package it.xfox.springboottest.test01.Repositories;


import it.xfox.springboottest.test01.Entities.Position;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.geo.Polygon;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

//repository with Position objects
//Some of these functions are not used but were used for tests.

public interface PositionRepository extends MongoRepository<Position, String> {

    //find the positions uploaded by the user with userid = id
    public List<Position> findByUserid(String id);

    //find the positions in the specified circle (center = location, radius = distance)
    public List<Position> findByLocationNear(Point location, Distance distance);

    //find the positions with coordinates contained in the polygon
    public List<Position> findByLocationWithin(Polygon polygon);

    //find the positions with coordinates in the poligon and timestamps in the time interval from start to end
    public List<Position> findByLocationWithinAndTimestampBetween(Polygon polygon, Date start, Date end);

    //find the position uploaded by the user with userid = id and with the most recent timestamp
    public Position findTopByUseridOrderByTimestampDesc(String id);
}
