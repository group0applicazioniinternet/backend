package it.xfox.springboottest.test01.Repositories;

import it.xfox.springboottest.test01.Entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

//repository with User objects
public interface UserRepository extends MongoRepository<User, String>{

    //find the user with username = username
    public User findByUsername(String username);

    //find the user with userid = id
    public User findById(String id);

    //find all registered users
    public List<User> findAll();
}
