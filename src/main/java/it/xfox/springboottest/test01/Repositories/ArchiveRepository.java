package it.xfox.springboottest.test01.Repositories;

import it.xfox.springboottest.test01.Entities.Archive;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

//repository of Archive objects
public interface ArchiveRepository extends MongoRepository<Archive, String> {

    //find archives uploaded by the user with userid = id
    public List<Archive> findByUserid(String id);

    //find the archive with archiveid = id
    public Archive findById(String id);

}
