package it.xfox.springboottest.test01;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.xfox.springboottest.test01.Entities.*;
import it.xfox.springboottest.test01.Implementations.UserDetailServiceImpl;
import it.xfox.springboottest.test01.Repositories.ArchiveRepository;
import it.xfox.springboottest.test01.Repositories.PositionRepository;
import it.xfox.springboottest.test01.Repositories.UserRepository;
import it.xfox.springboottest.test01.Utils.Response;
import it.xfox.springboottest.test01.Utils.UtilFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static it.xfox.springboottest.test01.Utils.UtilFunctions.hashPassword;


@RestController
@RequestMapping("/")
public class myController {

    @Autowired private UserDetailServiceImpl userDetailsService;
    @Autowired private UserRepository userRepository;
    @Autowired private PositionRepository positionRepository;
    @Autowired private ArchiveRepository archiveRepository;

    @RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    String get() {
        return "Homepage";
    }



    //---------- USER ----------


    //REGISTRATION of a new user
    //given its username and password
    /*
     REQUEST BODY:
     key = credentials
     value = {
         "username": "<username>",
         "password": "<password>"
     }
     */
    @RequestMapping(value = "/registration", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response registerUser(Principal principal, @RequestParam("credentials") String creds) throws IOException, ParseException {

        //initialize response object
        Response res = new Response(true, 0, "Ok");

        try {

            //retrieve username and password from "credentials" field in the body of the request
            ObjectMapper objectMapper = new ObjectMapper(); //create ObjectMapper instance
            JsonNode usernameNode = objectMapper.readTree(creds).path("username"); //obtain root node and retrieve the element in the specified path
            String username = usernameNode.toString().replace("\"", ""); //discard quotation marks
            JsonNode passwordNode = objectMapper.readTree(creds).path("password");
            String password = passwordNode.toString().replace("\"", "");


            //check if the username is already in use
            User user = userDetailsService.loadUserByUsername(username);
            if (user == null) { //username not in use

                //create the new user
                User u = new User(username, hashPassword(password), "USER");

                //save the new user in the User collection
                userRepository.save(u);

            } else { //username already in use
                res.setDefaultFields(false, 1, "Username already in use");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.getMessage());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }
    }




    //GET ARCHIVES UPLOADED by the user
    /*
     RESPONSE BODY FIELDS:
      "fields": {
         "archives": [
             {
                 "id": "<archiveId>",
                 "userid": "<userId>",
                 "elements": [
                     {
                         "location": {
                             "x": <x>,
                             "y": <y>
                         },
                         "timestamp": <timestamp>,
                         "valid": <true/false>
                     },
                     ...
                 ],
                 "appTimestamps": [
                     <appTimestamp>,
                     ...
                 ],
                 "appPoints": [
                     {
                         "x": <appX>,
                         "y": <appY>
                     },
                     ...
                 ],
                 "boughtCounter": <boughtCounter>,
                 "deleted": <true/false>
             },
             ...
         ]
     }
    */
    @RequestMapping(value = "/user/archives", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response getUserArchives(Principal principal) {

        Response res = new Response(true, 0, "Ok");

        try {

            //retrieve the logged user
            User user = userDetailsService.loadUserByUsername(principal.getName());

            if (user != null) { //Non si sa mai... :P

                //retrieve the list of archives uploaded by the user
                List<Archive> archives = archiveRepository.findByUserid(user.getId());

                if (archives.size() != 0) { //if the list is not empty

                    //add a field "archives" to the response object
                    //and set it to the list of archives found for the user
                    res.addField("archives", archives);

                } else { //archives list empty
                    res.setErrorcode(3);
                    res.setMessage("No archives found for this user");
                }

            } else { //user == null
                res.setDefaultFields(false, 1, "User not found");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.getMessage());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }
    }




    //GET ARCHIVES BOUGHT by the user
    /*
    RESPONSE BODY FIELDS:
    "fields": {
        "archives_bought": [
            {
                "id": "<archiveId>",
                "userid": "<userId>",
                "elements": [
                    {
                        "location": {
                            "x": <x>,
                            "y": <y>
                        },
                        "timestamp": <timestamp>,
                        "valid": <true/false>
                    },
                    ...
                ],
                "appTimestamps": [
                    <appTimestamp>,
                    ...
                ],
                "appPoints": [
                    {
                        "x": <appX>,
                        "y": <appY>
                    },
                    ...
                ],
                "boughtCounter": <boughtCounter>,
                "deleted": <true/false>
            },
            ...
        ]
    }
    */
    @RequestMapping(value = "/user/archives/bought", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response getUsertArchivesBough(Principal principal) {

        Response res = new Response(true, 0, "Ok");

        try {

            User user = userDetailsService.loadUserByUsername(principal.getName());
            if (user != null) {

                //retrieve the list of archiveids of the bought archives
                List<String> boughtArchiveIds = user.getArchiveBought();

                //list of bought archives
                List<Archive> boughtArchive = new ArrayList<>();

                //add the Archive objects to the list of bought archives
                for(String id : boughtArchiveIds) {
                    boughtArchive.add(archiveRepository.findById(id));
                }

                if (boughtArchive.size() != 0) {

                    //add a field "archives_bought" to the response object
                    //and set it to the list of bought archives
                    res.addField("archives_bought", boughtArchive);

                } else { //boughtArchives empty
                    res.setErrorcode(3);
                    res.setMessage("No archives bought found for this user");
                }
            } else { //user == null
                res.setDefaultFields(false, 1, "User not found");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.getMessage());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }
    }




    //UPLOAD AN ARCHIVE
    /*
     REQUEST BODY:
     key = positions
     value = {
         "archive": [
             {
                 "lat": <lat>,
                 "long": <long>,
                 "timestamp": "<timestamp>"
             },
             ...
         ]
     }
    */
    /*
    RESPONSE BODY FIELDS:
    "fields": {
        "addedElements": [
            {
                "location": {
                    "x": <x>,
                    "y": <y>
                },
                "timestamp": <timestamp>,
                "valid": <true/false>
            },
            ...
        ]
    }
    */
    @RequestMapping(value = "/user/archives", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response postUserArchives(Principal principal, @RequestParam("positions") String poss) throws IOException, ParseException {

        Response res = new Response(true, 0, "Ok");

        try {

            User user = userDetailsService.loadUserByUsername(principal.getName());
            if (user != null) {

                //retrieve an iterator over the elements in the "archive" field of the Request object
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode rootNode = objectMapper.readTree(poss).path("archive");
                Iterator<JsonNode> elements = rootNode.elements(); //Iterator of the elements

                //create a new Archive object
                Archive archive = new Archive(user.getId());

                //last element of the archive, for checks
                ArchiveElement lastElement = null;

                //list of the ArchiveElements added in the Archive
                List<ArchiveElement> addedElements = new ArrayList<>();

                while (elements.hasNext()) {
                    JsonNode jn = elements.next(); //for each element in the "archive" field...

                    //retrieve latitude and longitude, create a new Point
                    Point point = new Point(jn.get("lat").asDouble(), jn.get("long").asDouble());

                    //retrieve the timestamp, create a new Date
                    Date timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(jn.get("timestamp").asText());

                    //create the new ArchiveElement object with the specified lat, long and timestamp
                    ArchiveElement element = new ArchiveElement(point, timestamp);

                    //CHECKS FROM LAB 3
                    //not specified in the project specifications!!!
                    if (!element.isValid()) {
                        continue; //not valid
                    } else { //valid
                        if (lastElement == null) { //first element
                            archive.addElement(element);
                            addedElements.add(element);
                            lastElement = element;
                        } else { //not the first element -> check dates and velocity
                            if (!element.getTimestamp().after(lastElement.getTimestamp())) { //check dates
                                continue; //not sorted dates
                            } else { //valid (sorted) -> check velocity
                                Double velocity = element.evaluateVelocityOver(lastElement);
                                if (velocity >= 100) {
                                    continue; //not valid velocity

                                } else { //valid (velocity < 100)

                                    //add the element in the archive
                                    archive.addElement(element);

                                    //add the element in the array of added elements
                                    addedElements.add(element);

                                    //update last element
                                    lastElement = element;
                                }
                            }
                        }
                    }

                } //end while

                res.addField("addedElements", addedElements);

                //sort the approximate timestamps and positions in the Archive object
                archive.finalizeArchive();

                //save the new archive in the Archive collection
                archiveRepository.save(archive);


                //save the positions in the Position collection
                //we need to do this after saving the archive to obtain its id
                for (ArchiveElement e : addedElements) {
                    Position positionToSave = new Position(user.getId(), e.getLocation(), e.getTimestamp(), archive.getId());
                    positionRepository.save(positionToSave);
                }

            } else { //user = null
                res.setDefaultFields(false, 1, "User not found");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.getMessage());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }
    }




    //DELETE AN ARCHIVE given its archiveId
    /*
     REQUEST BODY:
     key = myData
     value = {
         "archiveId" : "<archiveId>"
     }
    */
    /*
     RESPONSE BODY FIELDS:
     "fields": {
         "deletedArchive": "<deletedArchiveId>"
     }
    */
    @RequestMapping(value = "/user/archives/delete", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response deleteUserArchive(Principal principal, @RequestParam("myData") String myData) throws IOException, ParseException {

        Response res = new Response(true, 0, "Ok");

        try {

            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(myData);
            String archiveid = rootNode.get("archiveId").toString().replace("\"", "");


            User user = userDetailsService.loadUserByUsername(principal.getName());
            if (user != null) {

                //list of archives of the user
                List<Archive> userArchives = archiveRepository.findByUserid(user.getId());

                //list of archiveids for the archives of the user
                List<String> userArchiveIds = new ArrayList<>();
                for (Archive a : userArchives) {
                    userArchiveIds.add(a.getId());
                }

                if (userArchiveIds.contains(archiveid)) { //check if the archive to delete belongs to the user

                    //retrieve the Archive object from the database
                    //and set its "deleted" field to true
                    Archive archiveFromDB = archiveRepository.findById(archiveid);
                    archiveFromDB.setDeleted(true);
                    archiveRepository.save(archiveFromDB);

                    res.addField("deletedArchive", archiveid);

                } else { //the archive does not belong to the user
                    res.setErrorcode(3);
                    res.setMessage("Archive not found");
                }
            } else { //user = null
                res.setDefaultFields(false, 1, "User not found");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.getMessage());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }

    }




    //RETRIEVE ARCHIVEIDS, APPROXIMATE POSITIONS AND APPROXIMATE TIMESTAMPS given time and polygon contraints
    //used to show the approximate data with the map as polygon constraint
    //and also to retrieve the selected data when the polygon has been drawn by the user

    /*
     REQUEST BODY:
     key = myData
     value = {
         "timestamp": {
             "start": "<start>",
             "end": "<end>"
         },
         "polygon": [
             {
                 "lat": <lat>,
                 "long": <long>
             },
             ...
         ]
     }
    */

    /*
     RESPONSE BODY FIELDS:
     "fields": {
         "approxData": {
             "archiveIds": [
                 "<archiveId>",
                 ...
             ],
             "users": {
                 "<userId>": {
                     "appTimestamps": [
                         <appTimestamp>,
                         ...
                     ],
                     "appPoints": [
                         {
                             "x": <x>,
                             "y": <y>
                         },
                         ...
                     ]
                 }
             }
         }
     }
    */
    @RequestMapping(value = "/user/approx/view", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response viewApproxPositions(Principal principal, @RequestParam("myData") String myData) throws IOException, ParseException {

        Response res = new Response(true, 0, "Ok");

        try {

            User user = userDetailsService.loadUserByUsername(principal.getName());
            if (user != null) {

                //retrieve the list of archives given the constraints
                List<Archive> archiveList = UtilFunctions.retrieveArchivesFromPolygonAndTimestamps(positionRepository, archiveRepository, myData);

                //structure for the Response field approxData
                //it contains:
                // the list of archiveids of the archives in the selected area and time interval
                // a map with key = userid and value = list of approximate positions and list of approximate timestamps
                ApproxData AD = new ApproxData();

                for (Archive a : archiveList) {

                    //add the data in the ApproxData object
                    if(!(a.getUserid().equals(user.getId()))) { //do not display archives uploaded by the user
                        if (!a.isDeleted()) { //do not display deleted archives
                            AD.addArchiveId(a.getId());
                            AD.addUserId_appData(a.getUserid(), a.getAppTimestamps(), a.getAppPoints());
                        }
                    }
                }

                res.addField("approxData", AD);

            } else { //user = null
                res.setDefaultFields(false, 1, "User not found");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.getMessage());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }
    }


    //OVERVIEW OF THE ARCHIVES THE USER IS ABOUT TO BUY with information about the already bought archives
    //given the list of archiveids of archives the user is about to buy
    //and the list of userids of users the user is interested in
    /*
    REQUEST BODY:
    key = myData
    value = {
        "archiveIds": [
            "<archiveId>",
            ...
        ],
        "userIds": [
            "<userId>",
            ...
        ]
    }
    */
    /*
    RESPONSE BODY FIELDS:
    "fields": {
        "data": [
            {
                "archiveId": "<archiveId>",
                "bought": <true/false>
            },
            ...
        ],
        "positionCounter": <counterTotPositions>
    }
     */
    @RequestMapping(value = "/user/buy/overview", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response buyPositionsOverview(Principal principal, @RequestParam("myData") String myData) throws IOException, ParseException {

        Response res = new Response(true, 0, "Ok");

        try {

            User user = userDetailsService.loadUserByUsername(principal.getName());
            if (user != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode rootNode = objectMapper.readTree(myData);

                //list with the archiveids of the archives the user is about to buy
                List<String> archiveIds = objectMapper.convertValue(rootNode.get("archiveIds"), ArrayList.class);

                //list with the userids that the user selected in the user selection checkbox
                //archives that are in the archiveIds list but belong to users that are not in this list must not be considered
                List<String> userIds = objectMapper.convertValue(rootNode.get("userIds"), ArrayList.class);

                //counter of the actual positions the user is about to buy
                int positionCounter = 0;

                //list of objects about the arhives the user is about to buy
                //each object contains the archiveid of the archive
                //and the information about the archive being already bought by the user
                List<Object> archivesFromDB = new ArrayList<>();

                //archiveids of the archives already bought by this user
                List<String> userBought = user.getArchiveBought();


                for (String AID : archiveIds) {

                    //for each archive
                    Archive a = archiveRepository.findById(AID);

                    if(userIds.contains(a.getUserid())) { //select only archives belonging to users in the list
                        positionCounter += a.getElements().size();
                        archivesFromDB.add(new Object() {
                            public final String archiveId = AID;
                            public final boolean bought = userBought.contains(AID);
                        });
                    }
                }

                res.addField("data", archivesFromDB);
                res.addField("positionCounter", positionCounter);
            } else { //user = null
                res.setDefaultFields(false, 1, "User not found");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.getMessage());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }
    }


    //PERFORM THE BUY ACTION
    //given same information as for the buyPositionsOverview
    /*
     REQUEST BODY:
     key = myData
     value = {
         "archiveIds": [
             "<archiveId>",
             ...
         ],
         "userIds": [
             "<userId>",
             ...
         ]
     }
    */
    /*
     RESPONSE BODY FIELDS:
     "fields": {
         "bought": [
             "<archiveId>",
             ...
         ]
     }
    */
    @RequestMapping(value = "/user/buy/final", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Response buyPositionsFinal(Principal principal, @RequestParam("myData") String myData) throws IOException, ParseException {

        Response res = new Response(true, 0, "Ok");

        try {

            User user = userDetailsService.loadUserByUsername(principal.getName());
            if(user != null) {

                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode rootNode = objectMapper.readTree(myData);

                //list with the archiveids of the archives the user is about to buy
                List<String> archiveIds = objectMapper.convertValue(rootNode.get("archiveIds"), ArrayList.class);

                //list with the userids that the user selected in the user selection checkbox
                List<String> userIds = objectMapper.convertValue(rootNode.get("userIds"), ArrayList.class);

                //list with the bought archives, to return in the Response object
                List<String> justBought = new ArrayList<>();

                //list of the archiveids of the already bought archives
                List<String> alreadyBought = user.getArchiveBought();

                for (String AID : archiveIds) {

                        if (!alreadyBought.contains(AID)) { //if not already bought by the user

                            Archive a = archiveRepository.findById(AID);
                            if (a != null) {
                                if (userIds.contains(a.getUserid())) {//if the archive exists and belongs to user that has to be considered

                                    //increment the number this archive has been bought
                                    a.setBoughtCounter(a.getBoughtCounter() + 1);
                                    archiveRepository.save(a);

                                    //add the archiveid to the list of already bought archives
                                    alreadyBought.add(AID);

                                    //add it the the list of just bought archives
                                    justBought.add(AID);
                                }
                            } else { //archive not found
                                res.setDefaultFields(true, 3, "At least one archive was not found");
                            }
                        } else { //already in userbought
                            res.setDefaultFields(true, 4, "At least one archive was already bought by this user");
                        }

                }

                userRepository.save(user);

                res.addField("bought", justBought);

            } else { //user = null
                res.setDefaultFields(false, 1, "User not found");
            }

            return res;

        } catch (Exception e) {
            res.setDefaultFields(false, 2, e.toString());
            String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
            System.out.println(stacktrace);
            return res;
        }
    }

}