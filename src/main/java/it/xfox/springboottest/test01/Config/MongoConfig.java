package it.xfox.springboottest.test01.Config;


import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
public class MongoConfig {
    //public @Bean MongoClient mongoClient() { return new MongoClient("localhost"); }
    //!Antonio usate: 192.168.99.100 al posto di localhost
    public @Bean MongoClient mongoClient() {
        return new MongoClient("192.168.99.100");
    }

    public @Bean MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), "myDB");
    }
}
