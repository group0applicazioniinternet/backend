package it.xfox.springboottest.test01.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;


//this class is requested by Spring Security
@Configuration
@EnableWebSecurity
public class ServerSecurityConfig extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    //how to authenticate the users:
    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        //This is where we are telling spring where to get user credential to authenticate them
        auth.userDetailsService(userDetailsService);
    }
}